#!/bin/bash
asciidoc-bib program.adoc --style chicago-author-date && asciidoctor -r asciidoctor-diagram program-ref.adoc && asciidoctor-pdf -r asciidoctor-diagram program-ref.adoc && rm *-ref.adoc
